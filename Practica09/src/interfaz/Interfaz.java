package interfaz;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.border.Border;

import funciones.CR;
import funciones.Imagenes;
import semaforos.BufferLimitado;
import semaforos.Consumidor;
import semaforos.Productor;

import javax.swing.JProgressBar;
import java.io.IOException;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.Font;
import javax.swing.JSpinner;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Interfaz {
	private JFrame frame;
	static Imagenes ObjImagen = new Imagenes();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interfaz window = new Interfaz();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	JLabel lblNewLabel = new JLabel("Proceso Consumidor");
	JLabel lblNewLabel_1 = new JLabel("Proceso Productor");
	JLabel lblProcesoConsumidor = new JLabel("");
	JLabel lblProcesoProductor = new JLabel("");
	static JProgressBar progressBar = new JProgressBar();
	static JLabel lblImgSemaforoConsumidor = new JLabel("");
	static JLabel lblImgSemaforoProductor = new JLabel("");
	static JLabel lblCRConsumidor = new JLabel("");
	static JLabel lblCRProductor = new JLabel("");
	static JLabel lblImgSemaforoProceso = new JLabel("");
	static JLabel lblCRConsumidor_2 = new JLabel("");
	static JLabel lblCRConsumidor_3 = new JLabel("");
	static JLabel lblCRProductor_2 = new JLabel("");
	static JLabel lblCRProductor_3 = new JLabel("");
	private final JLabel lblNewLabel_4 = new JLabel("Buffer de Elementos");
	

	/**
	 * Create the application.
	 * @throws IOException 
	 */
	private Interfaz() throws IOException {
		initialize();		
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws IOException 
	 */
	public void initialize() throws IOException {
		Border border = BorderFactory.createLineBorder(Color.BLACK, 1);
		frame = new JFrame();
		frame.setBounds(100, 100, 584, 587);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		lblNewLabel.setBounds(59, 0, 128, 51);
		frame.getContentPane().add(lblNewLabel);
		
		lblNewLabel_1.setBounds(400, 0, 114, 51);
		frame.getContentPane().add(lblNewLabel_1);
		
		lblProcesoConsumidor.setOpaque(true);
		lblProcesoConsumidor.setBackground(new Color(255, 250, 205));
		lblProcesoConsumidor.setBorder(border);
		lblProcesoConsumidor.setBounds(59, 34, 114, 125);
		lblImgSemaforoConsumidor.setIcon(Imagenes.obtenerImagen(2));
		frame.getContentPane().add(lblProcesoConsumidor);
		
		lblProcesoProductor.setOpaque(true);
		lblProcesoProductor.setBackground(new Color(100, 149, 237));
		lblProcesoProductor.setBorder(border);
		lblProcesoProductor.setBounds(400, 34, 114, 125);
		lblImgSemaforoProductor.setIcon(Imagenes.obtenerImagen(2));
		frame.getContentPane().add(lblProcesoProductor);
		
		progressBar = new JProgressBar(0,10); 
		progressBar.setStringPainted(true);
		progressBar.setOpaque(true);
		progressBar.setBackground(new Color(255, 255, 0));
		progressBar.setBounds(94, 383, 385, 38);
		frame.getContentPane().add(progressBar);
				
		lblImgSemaforoConsumidor.setBounds(178, 44, 72, 105);
		frame.getContentPane().add(lblImgSemaforoConsumidor);
		
		lblImgSemaforoProductor.setBounds(324, 44, 72, 105);
		frame.getContentPane().add(lblImgSemaforoProductor);
		
		//lblImgSemaforoConsumidor.setIcon(Imagenes.obtenerImagen(2));
		//lblImgSemaforoProductor.setIcon(Imagenes.obtenerImagen(2));
		
		lblCRConsumidor.setOpaque(true);
		lblCRConsumidor.setBackground(new Color(128, 128, 128));
		lblCRConsumidor.setBounds(10, 92, 10, 398);
		frame.getContentPane().add(lblCRConsumidor);
		
		lblCRProductor.setOpaque(true);
		lblCRProductor.setBackground(new Color(128, 128, 128));
		lblCRProductor.setBounds(550, 92, 10, 398);
		frame.getContentPane().add(lblCRProductor);
		
		lblImgSemaforoProceso.setBounds(249, 432, 72, 105);
		cambiarSemaforoVerde();
		frame.getContentPane().add(lblImgSemaforoProceso);
		
		lblCRConsumidor_2.setOpaque(true);
		lblCRConsumidor_2.setBackground(new Color(128, 128, 128));
		lblCRConsumidor_2.setBounds(10, 84, 49, 10);
		frame.getContentPane().add(lblCRConsumidor_2);
		
		lblCRConsumidor_3.setOpaque(true);
		lblCRConsumidor_3.setBackground(new Color(128, 128, 128));
		lblCRConsumidor_3.setBounds(10, 480, 240, 10);
		frame.getContentPane().add(lblCRConsumidor_3);
		lblCRProductor_2.setOpaque(true);
		lblCRProductor_2.setBackground(Color.GRAY);
		lblCRProductor_2.setBounds(513, 84, 47, 10);
		
		frame.getContentPane().add(lblCRProductor_2);
		lblCRProductor_3.setOpaque(true);
		lblCRProductor_3.setBackground(Color.GRAY);
		lblCRProductor_3.setBounds(318, 480, 240, 10);
		
		frame.getContentPane().add(lblCRProductor_3);
		
		JPanel panel = new JPanel();
		panel.setBounds(154, 233, 268, 139);
		panel.setBorder(border);
		frame.getContentPane().add(panel);
		
		JLabel lblRojo = new JLabel("     ");
		lblRojo.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblRojo.setOpaque(true);
		lblRojo.setBackground(Color.RED);
		
		JLabel lblNewLabel_2 = new JLabel("Rojo: No puede seguir trabajando.");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 12));
		
		JLabel lblNewLabel_3 = new JLabel("SEM\u00C1FOROS:");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.PLAIN, 13));
		
		JLabel lblVerde = new JLabel("     ");
		lblVerde.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblVerde.setOpaque(true);
		lblVerde.setBackground(Color.GREEN);
		
		JLabel lblNewLabel_5 = new JLabel("Verde: Se puede seguir trabajando.");
		lblNewLabel_5.setFont(new Font("Tahoma", Font.PLAIN, 12));
		
		JLabel lblCeldasDelBuffer = new JLabel("CELDAS DEL BUFFER:");
		lblCeldasDelBuffer.setFont(new Font("Tahoma", Font.PLAIN, 13));
		
		JLabel lblAmarillo = new JLabel("     ");
		lblAmarillo.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblAmarillo.setOpaque(true);
		lblAmarillo.setBackground(Color.YELLOW);
		
		JLabel lblNewLabel_6 = new JLabel("Amarillo: Celda sin elementos.");
		lblNewLabel_6.setFont(new Font("Tahoma", Font.PLAIN, 12));
		
		JLabel lblGris = new JLabel("     ");
		lblGris.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblGris.setOpaque(true);
		lblGris.setBackground(new Color(192, 192, 192));
		
		
		JLabel lblGris_2 = new JLabel("Gris: Celda con elementos.");
		lblGris_2.setFont(new Font("Tahoma", Font.PLAIN, 12));
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(lblRojo)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(lblNewLabel_2, GroupLayout.DEFAULT_SIZE, 224, Short.MAX_VALUE))
						.addComponent(lblNewLabel_3)
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(lblVerde)
							.addGap(10)
							.addComponent(lblNewLabel_5, GroupLayout.DEFAULT_SIZE, 224, Short.MAX_VALUE))
						.addComponent(lblCeldasDelBuffer)
						.addGroup(gl_panel.createSequentialGroup()
							.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addComponent(lblAmarillo)
								.addComponent(lblGris))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addComponent(lblGris_2, GroupLayout.DEFAULT_SIZE, 224, Short.MAX_VALUE)
								.addComponent(lblNewLabel_6, GroupLayout.DEFAULT_SIZE, 224, Short.MAX_VALUE))))
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(8)
					.addComponent(lblNewLabel_3)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblRojo)
						.addComponent(lblNewLabel_2))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblVerde)
						.addComponent(lblNewLabel_5))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblCeldasDelBuffer)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblAmarillo)
						.addComponent(lblNewLabel_6))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblGris)
						.addComponent(lblGris_2))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		panel.setLayout(gl_panel);
		lblNewLabel_4.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel_4.setBounds(90, 444, 135, 14);
		
		frame.getContentPane().add(lblNewLabel_4);
		
		JSpinner n_consumos = new JSpinner();
		n_consumos.setBounds(255, 31, 65, 20);
		frame.getContentPane().add(n_consumos);
		
		JSpinner n_productos = new JSpinner();
		n_productos.setBounds(255, 74, 65, 20);
		frame.getContentPane().add(n_productos);
		
		JSpinner time_produccion = new JSpinner();
		time_produccion.setBounds(255, 120, 65, 20);
		frame.getContentPane().add(time_produccion);
		
		JSpinner time_consumo = new JSpinner();
		time_consumo.setBounds(255, 168, 65, 20);
		frame.getContentPane().add(time_consumo);
		
		JButton btnComenzar = new JButton("Comenzar");
		btnComenzar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				BufferLimitado buffer = new BufferLimitado();
				Productor productor = new Productor( buffer, (Integer)n_productos.getValue(), (Integer)time_produccion.getValue() );
				Consumidor consumidor = new Consumidor( buffer, (Integer)n_consumos.getValue(), (Integer)time_consumo.getValue() );
			}
		});
		btnComenzar.setBounds(222, 199, 135, 23);
		frame.getContentPane().add(btnComenzar);
		
		JLabel lblNumConsumidores = new JLabel("num. consumos");
		lblNumConsumidores.setFont(new Font("Tahoma", Font.PLAIN, 9));
		lblNumConsumidores.setBounds(256, 17, 72, 17);
		frame.getContentPane().add(lblNumConsumidores);
		
		JLabel lblNumProductos = new JLabel("num. productos");
		lblNumProductos.setFont(new Font("Tahoma", Font.PLAIN, 9));
		lblNumProductos.setBounds(258, 57, 63, 17);
		frame.getContentPane().add(lblNumProductos);
		
		JLabel lblT = new JLabel("t_producci\u00F3n");
		lblT.setFont(new Font("Tahoma", Font.PLAIN, 9));
		lblT.setBounds(260, 105, 59, 17);
		frame.getContentPane().add(lblT);
		
		JLabel lblTconsumo = new JLabel("t_consumo");
		lblTconsumo.setFont(new Font("Tahoma", Font.PLAIN, 9));
		lblTconsumo.setBounds(265, 151, 49, 17);
		frame.getContentPane().add(lblTconsumo);

	}
	static public void cambiarSemaforo() throws IOException{
		int op 		= Imagenes.regresarTipoImagen();
		String nom 	= Imagenes.regresarConsumidorOProductor();
		if(nom == "productor"){
			lblImgSemaforoProductor.setIcon(Imagenes.obtenerImagen(op));
		}else if(nom == "consumidor"){
			lblImgSemaforoConsumidor.setIcon(Imagenes.obtenerImagen(op));
		}else if(nom == null && op==0){
			lblImgSemaforoProductor.setIcon(Imagenes.obtenerImagen(0));
			lblImgSemaforoConsumidor.setIcon(Imagenes.obtenerImagen(0));
		}
	}
	
	static public void cambiarCR(){
		int tipoCR 		= CR.regresarTipoCR();
		String tipoFD 	= CR.regresarFetchODeposit();
		try {
			lblImgSemaforoProceso.setIcon(Imagenes.obtenerImagen(1));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(tipoCR == 1 && tipoFD == "fetch"){
			lblCRProductor.setBackground(Color.RED);
			lblCRProductor_2.setBackground(Color.RED);
			lblCRProductor_3.setBackground(Color.RED);
		}else if(tipoCR == 1 && tipoFD == "deposit"){
			lblCRConsumidor.setBackground(Color.RED);
			lblCRConsumidor_2.setBackground(Color.RED);
			lblCRConsumidor_3.setBackground(Color.RED);
		}else if(tipoCR == 2 && tipoFD == "fetch"){
			lblCRProductor.setBackground(new Color(128, 128, 128));
			lblCRProductor_2.setBackground(new Color(128, 128, 128));
			lblCRProductor_3.setBackground(new Color(128, 128, 128));
		}else if(tipoCR == 2 && tipoFD == "deposit"){
			lblCRConsumidor.setBackground(new Color(128, 128, 128));
			lblCRConsumidor_2.setBackground(new Color(128, 128, 128));
			lblCRConsumidor_3.setBackground(new Color(128, 128, 128));
		}
	}
	
	static public void agregarItemBarra(int n){
		progressBar.setValue(progressBar.getValue()+n);
		String num=String.valueOf(progressBar.getValue());
		progressBar.setString(num);
		progressBar.repaint();
	}
	
	static public void quitarItemBarra(int n){
		progressBar.setValue(progressBar.getValue()-n);
		String num=String.valueOf(progressBar.getValue());
		progressBar.setString(num);
		progressBar.repaint();
	}
	
	static public void cambiarSemaforoVerde(){
		try {
			lblImgSemaforoProceso.setIcon(Imagenes.obtenerImagen(2));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
