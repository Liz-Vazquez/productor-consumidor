package semaforos;

import java.io.IOException;

import funciones.Imagenes;
import interfaz.Interfaz;

public class Consumidor implements Runnable {
	BufferLimitado b = null;
	int num_consumos = 0, t_consumo = 0;
	int i=0;
	public Consumidor( BufferLimitado initb, int num_c, int t_c ) {
	b = initb;
	num_consumos 	= num_c;
	t_consumo 		= t_c;
	new Thread( this ).start();
	}
	public void run() {
	double item;
	while( i < num_consumos ) {
	item = b.fetch();
	System.out.println( "Art�culo extra�do " + item );
	Imagenes.obtenerTipoImagen(2, "consumidor"); // obtiene el sem�foro en color verde para el consumidor
	try {
		Interfaz.cambiarSemaforo();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	Imagenes.obtenerTipoImagen(1, "productor"); // obtiene el sem�foro en color rojo para el productor
	try {
		Interfaz.cambiarSemaforo();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	Util.mySleep(t_consumo);
	i++;
	}
	
	}
}
