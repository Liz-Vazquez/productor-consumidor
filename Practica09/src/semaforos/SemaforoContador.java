package semaforos;

import interfaz.Interfaz;

public class SemaforoContador {
	int value;
	public SemaforoContador( int initValue)
	{
		value = initValue;
	}
	public synchronized void P() {
		value--;
		System.out.println("resta: "+value);
		if( value < 0 )
			Util.myWait( this );
		Interfaz.quitarItemBarra(value);
	}
	public synchronized void V() {
		value++;
		System.out.println("aumento: "+value);
		if( value <= 0 )
			notify();
		Interfaz.agregarItemBarra(value);
	}
}
