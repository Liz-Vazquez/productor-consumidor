package semaforos;

import funciones.CR;

public class BufferLimitado {
	final int size = 10;
	double buffer[] = new double[size];
	int inBuf = 0, outBuf = 0;
	SemaforoBinario mutex = new SemaforoBinario(true);
	SemaforoContador isEmpty = new SemaforoContador(0);
	SemaforoContador isFull = new SemaforoContador( size );
	
	
	public void deposit( double value )
	{
		CR.obtenerFetchODeposit("deposit");
		isFull.P(); // espera si el buffer est� lleno
		mutex.P(); // asegura la exclusi�n mutua
		buffer[inBuf] = value;
		inBuf = (inBuf + 1) % size;
		mutex.V();
		isEmpty.V(); // notifica a alg�n consumidor en espera
	}
	
	public double fetch()
	{
		CR.obtenerFetchODeposit("fetch");
		double value;
		isEmpty.P(); // esperar si el buffer est� vac�o
		mutex.P(); // asegura la exclusi�n mutua
		value = buffer[outBuf]; // lee desde el buffer
		outBuf = (outBuf+1) % size;
		mutex.V();
		isFull.V(); // notifica a cualquier productor en espera
		return value;
	}
}

