package semaforos;
import java.io.IOException;
import java.util.Random;

import funciones.Imagenes;
import interfaz.Interfaz;
public class Productor implements Runnable {
	BufferLimitado b = null;
	int num_productos = 0, t_produccion = 0;
	int i=0;
	public Productor( BufferLimitado initb, int num_p, int t_p ) {
	b = initb;
	num_productos 	= num_p;
	t_produccion 	= t_p;
	new Thread( this ).start();
	}
	public void run() {
	double item;
	Random r = new Random();
	while( i < num_productos ) {
	item = r.nextDouble();
	System.out.println( "Art�culo producido " + item );
	Imagenes.obtenerTipoImagen(2, "productor"); // obtiene el sem�foro en color verde para el productor
	try {
		Interfaz.cambiarSemaforo();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	Imagenes.obtenerTipoImagen(1, "consumidor"); // obtiene el sem�foro en color rojo para el consumidor
	try {
		Interfaz.cambiarSemaforo();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	b.deposit( item );
	Util.mySleep(t_produccion);
	i++;
	}
	}


}
