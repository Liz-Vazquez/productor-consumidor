package semaforos;

import funciones.CR;
import interfaz.Interfaz;

public class SemaforoBinario {
	boolean value;
	SemaforoBinario( boolean initValue )
	{
		value = initValue;
	}
	public synchronized void P()
	{
		while( value == false )
		Util.myWait(this); //en cola de procesos bloqueados
		CR.obtenerCR(1); //Entra a la CR
		Interfaz.cambiarCR();
		Util.mySleep(20);
		
		value = false;

	}
	public synchronized void V()
	{
		CR.obtenerCR(2); //Sale de la CR
		Interfaz.cambiarCR();
		Interfaz.cambiarSemaforoVerde();
		Util.mySleep(40);
		value = true;
		notify();
	}
}
