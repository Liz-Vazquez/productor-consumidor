package funciones;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

public class Imagenes {
	static int tipoImg;
	static String nombre;
	public static ImageIcon obtenerImagen(int tipo) throws IOException{
		ImageIcon imagen = null;
		BufferedImage img = null;
		if(tipo == 1){ // Imagen sem�foro rojo
			String pathImagen = "Resources\\semaforoRojo.jpg";
			File archivo = new File(pathImagen);
			img = ImageIO.read(archivo);
			imagen = new ImageIcon(img.getScaledInstance(72, 105, Image.SCALE_SMOOTH));
		}else if(tipo == 2){ // Imagen sem�foro verde
			String pathImagen = "Resources\\semaforoVerde.jpg";
			File archivo = new File(pathImagen);
			img = ImageIO.read(archivo);
			imagen = new ImageIcon(img.getScaledInstance(72, 105, Image.SCALE_SMOOTH));
		}else if(tipo == 0){
			String pathImagen = "Resources\\semaforoVerde.jpg";
			File archivo = new File(pathImagen);
			img = ImageIO.read(archivo);
			imagen = new ImageIcon(img.getScaledInstance(72, 105, Image.SCALE_SMOOTH));
		}
		return imagen;
	}
	
	public static void obtenerTipoImagen(int tipo, String nom){
		/*System.out.println("tipo de imagen obtenida: "+tipo);
		System.out.println("Nombre obtenido: "+nom);*/
		tipoImg		=	tipo;
		nombre 		= 	nom;
	}
	
	public static int regresarTipoImagen(){
		/*System.out.println("tipo de imagen regresada: "+tipoImg);
		System.out.println("Nombre regresado: "+nombre);*/
		return tipoImg;
	}
	public static String regresarConsumidorOProductor(){
		return nombre;
	}
	

}
